package 

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils

@Secured(['ROLE_USER'])
class Controller extends BaseController {

    def index() {
    
    }

    def show() {
    
    }

    def save() {
    
    }

    def delete() {
    
    }

}