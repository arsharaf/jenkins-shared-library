#!/usr/bin/env groovy
def call(){
        echo "Starting build docker image for $BRANCH_NAME branch.."
        sh 'docker build -t arsharaf/demoapp:1.0 .'
        sh "echo $DOCKER_CREDS_PSW | docker login -u $DOCKER_CREDS_USR  --password-stdin"
        sh 'docker push arsharaf/demoapp:1.0 '
}
