#! /usr/bin/env groovy
def call(){
    echo 'Starting build jar for ${BRANCH_NAME} branch..'
    sh 'mvn --version'
    sh 'mvn package'
}
